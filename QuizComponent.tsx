import { useDispatch, useSelector } from "react-redux";
import { Button, Progress, Radio, RadioChangeEvent, Space } from "antd";
import { Answer, Question } from "../types";
import { RootState } from "../store";
import { useEffect, useState } from "react";
import { setIsQuizFinished, setTotalQuestions, setUserAnswers, setUserScore } from "../reducers/QuizReducer";

const QuizComponent = () => {
  const dispatch = useDispatch();
  const QUESTION_WITH_SUBQUIESTION_INDEX = 2;
  const questions: Question[] = useSelector((state: RootState) => state.quiz.questions);
  const subQuestions: Question[] = useSelector((state: RootState) => state.quiz.subQuestions);
  const answers: Answer[] = useSelector((state: RootState) => state.quiz.userAnswers);
  const score: number = useSelector((state: RootState) => state.quiz.userScore);
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [currentSubquestionIndex, setCurrentSubquestionIndex] = useState<number>(0);
  const [values, setValues] = useState<string>('')

  useEffect(() => {
    if (questions.length > 0 && subQuestions.length > 0) {
      dispatch(setTotalQuestions(questions.length + subQuestions.length));
    }
  }, [questions, subQuestions])

  const isSubQuestion = currentIndex === QUESTION_WITH_SUBQUIESTION_INDEX  && currentSubquestionIndex < subQuestions.length;

  const goToNextQuestion = () => {
    const answer = currentQuestion.answers.find((item) => item.answer === values);
    if (!values || !answer) {
      console.log(`no answer ${answer}`)
      return;
    }
    dispatch(setUserAnswers([...answers, answer]));
    if (answer.isCorrect) {
      dispatch(setUserScore(score + currentQuestion.score));
    }
    
    if (isSubQuestion) {
      setCurrentSubquestionIndex(currentSubquestionIndex + 1);
    } else {
      setCurrentIndex(currentIndex + 1);
    }

    setValues('');
    if (questions.length > 0 && (questions.length - 1) <= currentIndex) {
      dispatch(setIsQuizFinished(true));
      return;
    }
  }

  const onChange = (e: RadioChangeEvent) => {
    setValues(e.target.value);
  };

  if (questions.length > 0 && (questions.length - 1) <= currentIndex) {
    dispatch(setIsQuizFinished(true));
    return <></>;
  }
  if (questions.length === 0) {
    return <></>;
  }

  const currentSubQuestion = subQuestions[currentSubquestionIndex];
  const currentQuestion = isSubQuestion ? currentSubQuestion : questions[currentIndex];

  const isSub = isSubQuestion ? 'SUB' : '';
  return (
    <Space direction="vertical">
      <Progress percent={(currentIndex * 100) / questions.length} />
      <h1>{isSub} {currentQuestion.question}</h1>
      <Radio.Group onChange={onChange} value={values}>
        <Space direction="vertical">
        {currentQuestion?.answers.map((item) => (
          <Radio value={item.answer}>{item.answer}</Radio>
        ))}
        </Space>
      </Radio.Group>
      <Button type="primary" onClick={goToNextQuestion}>Go to next question</Button>
    </Space>
  );
}

export default QuizComponent;
