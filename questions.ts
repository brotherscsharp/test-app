export const MOCK_QUESTIONS = [
  {
    "question": "What is the purpose of a constructor in Java?",
    score: 2,
    "answers": [
      {
        "answer": "To initialize the newly created object.",
        "isCorrect": true
      },
      {
        "answer": "To define the class methods.",
        "isCorrect": false
      },
      {
        "answer": "To create a new object instance.",
        "isCorrect": false
      },
      {
        "answer": "To declare variables.",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What does MVC stand for in software architecture?",
    score: 2,
    "answers": [
      {
        "answer": "Model View Controller",
        "isCorrect": true
      },
      {
        "answer": "Most Valuable Code",
        "isCorrect": false
      },
      {
        "answer": "Multiple View Components",
        "isCorrect": false
      },
      {
        "answer": "Modified View Control",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What is the purpose of version control systems like Git?",
    score: 5,
    "answers": [
      {
        "answer": "To track changes in source code over time.",
        "isCorrect": true
      },
      {
        "answer": "To compile code into executable files.",
        "isCorrect": false
      },
      {
        "answer": "To optimize database queries.",
        "isCorrect": false
      },
      {
        "answer": "To design user interfaces.",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What does OOP stand for in programming?",
    score: 1,
    "answers": [
      {
        "answer": "Object-Oriented Programming",
        "isCorrect": true
      },
      {
        "answer": "Operation Overloading Principle",
        "isCorrect": false
      },
      {
        "answer": "Optimized Output Process",
        "isCorrect": false
      },
      {
        "answer": "Open-Source Operating Platform",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What is the purpose of the 'finally' block in a Java try-catch-finally statement?",
    score: 4,
    "answers": [
      {
        "answer": "To execute code regardless of an exception.",
        "isCorrect": true
      },
      {
        "answer": "To catch exceptions.",
        "isCorrect": false
      },
      {
        "answer": "To handle user input.",
        "isCorrect": false
      },
      {
        "answer": "To define class methods.",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What does API stand for in programming?",
    score: 2,
    "answers": [
      {
        "answer": "Application Programming Interface",
        "isCorrect": true
      },
      {
        "answer": "Advanced Programming Instruction",
        "isCorrect": false
      },
      {
        "answer": "Application Protocol Interface",
        "isCorrect": false
      },
      {
        "answer": "Advanced Protocol Instruction",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What does SQL stand for in database management?",
    score: 1,
    "answers": [
      {
        "answer": "Structured Query Language",
        "isCorrect": true
      },
      {
        "answer": "Sequential Query Language",
        "isCorrect": false
      },
      {
        "answer": "Structured Queueing Language",
        "isCorrect": false
      },
      {
        "answer": "Simple Query Logic",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What is the purpose of the 'this' keyword in Java?",
    score: 3,
    "answers": [
      {
        "answer": "To refer to the current object instance.",
        "isCorrect": true
      },
      {
        "answer": "To access static methods.",
        "isCorrect": false
      },
      {
        "answer": "To initialize variables.",
        "isCorrect": false
      },
      {
        "answer": "To define class constructors.",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What is a binary search?",
    score: 4,
    "answers": [
      {
        "answer": "A search algorithm that finds the position of a target value within a sorted array.",
        "isCorrect": true
      },
      {
        "answer": "A search algorithm that finds the minimum value in an unsorted array.",
        "isCorrect": false
      },
      {
        "answer": "A search algorithm that finds the maximum value in an unsorted array.",
        "isCorrect": false
      },
      {
        "answer": "A search algorithm that compares each element in an array with the target value.",
        "isCorrect": false
      }
    ]
  }
];

export const MOCK_SUB_QUESTIONS = [
  {
    "question": "What is the difference between == and === operators in JavaScript?",
    score: 1,
    "answers": [
      {
        "answer": "== checks only for equality in value, while === checks for equality in value and data type.",
        "isCorrect": true
      },
      {
        "answer": "== and === are interchangeable and behave the same way.",
        "isCorrect": false
      },
      {
        "answer": "== checks for equality in value and data type, while === checks only for equality in value.",
        "isCorrect": false
      },
      {
        "answer": "== and === are used for conditional statements and loops.",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What is the purpose of the 'static' keyword in Java?",
    score: 1,
    "answers": [
      {
        "answer": "To create a class-level variable or method that belongs to the class, rather than to instances of the class.",
        "isCorrect": true
      },
      {
        "answer": "To create an instance-level variable or method that belongs to instances of the class.",
        "isCorrect": false
      },
      {
        "answer": "To define class constructors.",
        "isCorrect": false
      },
      {
        "answer": "To access non-static variables or methods.",
        "isCorrect": false
      }
    ]
  },
  {
    "question": "What is the purpose of a constructor in C#?",
    score: 1,
    "answers": [
      {
        "answer": "To initialize the newly created object.",
        "isCorrect": true
      },
      {
        "answer": "To define the class methods.",
        "isCorrect": false
      },
      {
        "answer": "To create a new object instance.",
        "isCorrect": false
      },
      {
        "answer": "To declare variables.",
        "isCorrect": false
      }
    ]
  }
];
