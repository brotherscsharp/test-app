/* eslint-disable */
import { createAsyncThunk, createSlice, PayloadAction, unwrapResult } from '@reduxjs/toolkit';
import { getQuestions, getSubQuestions } from '../services/QuizSevice';

import type { Answer, Question, QuizState } from '../types';

export const INITIAL_STATE: QuizState = {
    questions: [],
    subQuestions: [],
    userAnswers: [],
    userScore: 0,
    totalQuestions: 0,
    isQuizFinished: false,
};

export const getQuizQuestions = createAsyncThunk(
    'quiz/getQuizQuestions',
    async (payload: {}) => {
      try {
        return await getQuestions();
      } catch (error) {
        return { error };
      }
    },
);

export const getQuizSubQuestions = createAsyncThunk(
    'quiz/getQuizSubQuestions',
    async (payload: {}) => {
      try {
        return await getSubQuestions();
      } catch (error) {
        return { error };
      }
    },
);

const quizReducer = createSlice({
  name: 'quizReducer',
  initialState: INITIAL_STATE,
  reducers: {
    setQuestions: (state: QuizState, action: PayloadAction<Question[]>) => {
      return { ...state, questions: action.payload };
    },
    setIsQuizFinished: (state: QuizState, action: PayloadAction<boolean>) => {
      return { ...state, isQuizFinished: action.payload };
    },
    setUserAnswers: (state: QuizState, action: PayloadAction<Answer[]>) => {
      return { ...state, userAnswers: action.payload };
    },
    setUserScore: (state: QuizState, action: PayloadAction<number>) => {
      return { ...state, userScore: action.payload };
    },
    setTotalQuestions: (state: QuizState, action: PayloadAction<number>) => {
      return { ...state, totalQuestions: action.payload };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getQuizQuestions.fulfilled, (state, action) => {
      const questions = unwrapResult(action);
      if (questions) {
        return { ...state, questions: questions as Question[] };
      }
      return state;
    });
    builder.addCase(getQuizSubQuestions.fulfilled, (state, action) => {
      const questions = unwrapResult(action);
      if (questions) {
        return { ...state, subQuestions: questions as Question[] };
      }
      return state;
    });
  },
});

export const {
    setQuestions,
    setIsQuizFinished,
    setUserAnswers,
    setUserScore,
    setTotalQuestions
} = quizReducer.actions;

export default quizReducer.reducer;
/* eslint-enable */
