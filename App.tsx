import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Layout, MenuProps, theme } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import './App.css';
import { getQuizQuestions, getQuizSubQuestions } from './reducers/QuizReducer';
import { RootState } from './store';
import QuizComponent from './components/QuizComponent';
import QuizResult from './components/QuizResult';

const { Header, Content, Sider } = Layout;

const items2: MenuProps['items'] = [UserOutlined].map(
  (icon, index) => {
    const key = String(index + 1);

    return {
      key: `sub${key}`,
      icon: React.createElement(icon),
      label: `Menu`,

      children: [
        {
          key: 'tests',
          label: `Tests`,
        },
        {
          key: 'results',
          label: `Results`,
        },
      ],
    };
  },
);

function App() {
  const dispatch = useDispatch();
  const isQuizFinished: boolean = useSelector((state: RootState) => state.quiz.isQuizFinished);
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  useEffect(() => {
    // @ts-ignore
    dispatch(getQuizQuestions({}));
    // @ts-ignore
    dispatch(getQuizSubQuestions({}));
  }, []);

  return (
    <Layout>
    <Header style={{ display: 'flex', alignItems: 'center' }}>
      <div className="demo-logo" />
    </Header>
    <Layout>
      
      <Layout style={{ padding: '0 24px 24px' }}>
        <Content
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
            justifyContent: 'center',
            display: 'flex',
          }}
        >
          <div style={{ width: '80%'}}>
            {isQuizFinished ? (
              <QuizResult />
            ) : (
              <QuizComponent />
            )}
          </div>
        </Content>
      </Layout>
    </Layout>
  </Layout>
  );
}

export default App;
