import { MOCK_QUESTIONS, MOCK_SUB_QUESTIONS } from "../mock/questions";
import type { Question } from "../types";

export const getQuestions = (): Promise<Question[]> => {
  return new Promise((resolve) => {
    resolve(MOCK_QUESTIONS);
  });
}

export const getSubQuestions = (): Promise<Question[]> => {
  return new Promise((resolve) => {
    resolve(MOCK_SUB_QUESTIONS);
  });
}

