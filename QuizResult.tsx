import { useDispatch, useSelector } from "react-redux";
import { Button, Space } from "antd";
import { Answer, Question } from "../types";
import { RootState } from "../store";
import { setIsQuizFinished } from "../reducers/QuizReducer";

const QuizResult = () => {
  const dispatch = useDispatch();
  const answers: Answer[] = useSelector((state: RootState) => state.quiz.userAnswers);
  const score: number = useSelector((state: RootState) => state.quiz.userScore);
  const totalQuestions: number = useSelector((state: RootState) => state.quiz.totalQuestions);
  const questions: Question[] = useSelector((state: RootState) => state.quiz.questions);

  const startQuizAgain = () => {
    dispatch(setIsQuizFinished(false));
  }

  const checkResult = () => {
    const correct = answers.filter((items) => items.isCorrect);
    return correct.length;
  }

  return (
    <Space direction="vertical">
      <h1>Quiz is finished</h1>
      <div>{`${((checkResult() / totalQuestions) * 100).toFixed()}%`}</div> 
      <div>Score: {score}</div> 
      <Button type="primary" onClick={startQuizAgain}>Start Again</Button>
    </Space>
  );
}

export default QuizResult;