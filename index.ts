export type QuizState = { 
  questions: Question[];
  subQuestions: Question[];
  userAnswers: Answer[];
  userScore: number;
  totalQuestions: number;
  isQuizFinished: boolean;
};

export type Question = {
  question: string;
  score: number;
  answers: Answer[];
}

export type Answer = {
  answer: string;
  isCorrect: boolean;
}